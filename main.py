from kivy.app import App
from kivy.garden.iconfonts import register

from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.scrollview import ScrollView
from kivy.uix.label import Label
from kivy.uix.widget import Widget 
from kivy.uix.accordion import AccordionItem
from kivy.uix.carousel import Carousel
from kivy.uix.image import AsyncImage
from kivy.properties import StringProperty, ObjectProperty
from kivy.core.window import Window
from kivy.metrics import dp, sp
from kivy.uix.screenmanager import ScreenManager, Screen

from kivy.lang import Builder
from kivy.uix.behaviors import ButtonBehavior
from kivy.properties import StringProperty, NumericProperty, ListProperty, BooleanProperty
from kivy.metrics import sp, dp
from kivy.utils import rgba
from kivy.utils import get_color_from_hex as rgb
from kivy.graphics.vertex_instructions import Line
from kivy.graphics.context_instructions import Color
from os.path import dirname, join

from kivy.clock import Clock

# set windows size
Window.size = (320, 645)
Window.borderless = True
Window.clearcolor = (255, 255, 255, 1)

from app.utils.navigation_screen_manager import NavigationScreenManager
from app.pages.signin import SigninScreen
from app.pages.signup import SignupScreen
from app.pages.cart import CartScreen
from app.pages.product import ProductScreen 
from app.pages.tabar import BoxLayoutWithActionBar 



class MainMenu(BoxLayout):
    def __init__(self, **kwargs):
        super(MainMenu, self).__init__(**kwargs)

        for i in range(5):
            btn = Button(text=f'Button {i}')
            #btn.on_press = nav.push(self,screen_name="screen3")
            btn.id = f"btn{i}"
            self.add_widget(btn)


class MyScreenManager(NavigationScreenManager):
     #nav = NavigationScreenManager
     #!print(dir(nav))
     #nav.current = "SignupScreen"
     pass

import os
path = os.getcwd()
with open(f"{path}/quolia.kv", encoding='utf8') as f:
    Builder.load_string(f.read())

class MainApp(App):
    manager = ObjectProperty(None)

    def build(self):
        self.manager = MyScreenManager()
        return self.manager


MainApp().run()  

#if __name__ == '__main__':
    #register('MatIcons', join (dirname(__file__), 'app/assets/fonts/Iconic-Font.ttf'), join(dirname(__file__), 'app/assets/fonts/zmd.fontd'))
