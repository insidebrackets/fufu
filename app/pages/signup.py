from kivy.lang import Builder
from kivy.uix.boxlayout import BoxLayout

import os
path = os.getcwd()
project_path = path + '/app/pages/'
Builder.load_file(project_path + 'signup.kv')

class SignupScreen(BoxLayout):
    pass
