from kivy.lang import Builder
from kivy.uix.boxlayout import BoxLayout
from kivy.properties import StringProperty 
import os
path = os.getcwd()
project_path = path + '/app/pages/'
Builder.load_file(project_path + 'tabar.kv')

class BoxLayoutWithActionBar(BoxLayout):
    title = StringProperty()

